# Ansible role for TFTP Server installation

## Introduction

[TFTP-HPA](http://www.kernel.org/pub/software/network/tftp/) is a TFTP server implementation,
automatically spawned when needed using systemd socket activation.

This role installs and configure the server. Setting up the firewall is done by setting the list os allowed source addresses using the `allowed_subnets` variable.

